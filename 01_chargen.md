Chargen
=======

1.  Choose a Homeworld (Lol you don't roll stats, you pick homeworld)

  World Type         Toughness   Agility   Strength   Intelligence   Felllowship   Willpower
  ------------------ ----------- --------- ---------- -------------- ------------- -----------
  Agri-World         Tuf +1      Agi -1    Str +2     Int +0         Fel +1        Wil +0
  Daemon World       Tuf +0      Agi +1    Str +0     Int +1         Fel -1        Wil +2
  Death World        Tuf +1      Agi +2    Str +1     Int +0         Fel -1        Wil +0
  Feral World        Tuf +2      Agi +1    Str +1     Int +0         Fel -1        Wil +0
  Feudal World       Tuf +0      Agi +2    Str +1     Int -1         Fel +1        Wil +0
  Forge World        Tuf +1      Agi +1    Str +0     Int +2         Fel -1        Wil +0
  Frontier World     Tuf +0      Agi +2    Str +1     Int +1         Fel -1        Wil +0
  Garden World       Tuf -1      Agi +1    Str +0     Int +0         Fel +2        Wil +1
  Highborn           Tuf -1      Agi +0    Str +0     Int +1         Fel +2        Wil +1
  Hiveworld          Tuf +0      Agi +2    Str +0     Int +1         Fel +1        Wil -1
  Penal Colony       Tuf +2      Agi +1    Str +1     Int +0         Fel -1        Wil +0
  Quarantine World   Tuf +0      Agi +0    Str -1     Int +3         Fel +0        Wil +0
  Research Station   Tuf +0      Agi +0    Str +0     Int +3         Fel -1        Wil +0
  Shrine World       Tuf +0      Agi -1    Str +0     Int +1         Fel +2        Wil +1
  Voidborn           Tuf +0      Agi +1    Str -1     Int +2         Fel +0        Wil +1

You have 1 Hit Die (d6) extra HD equal to your TUF. Roll all your HD and keep a number equal to your rank to determine your HP. When you rest and consume some rations, you may re-roll your HP if you like. If you rest in comfort and safety, roll +1 HD. If you are attended by a healer, roll +1 HD.

2.  Choose a Background (Skill 1)
    -   Adeptus Administratum gain Bureaucracy
    -   Adeptus Arbites gain Awareness
    -   Adeptus Astra Telepathica gain Psyniscience
    -   Adeptus Ministorum gain Leadership
    -   Imperial Guard or Navy gain Survival

3.  Choose a Role (Skill 2 and Ability 1&2)
    -   Adeptus Mechanicus gain Tech-Use and two abilities from this list:
        -   Speak With Machine Spirit (you can converse with and attempt to command technology)
        -   Control(You can remote control technology with your mind from 200 yards away)
        -   Familiar(1 useful mind-linked floating servo-skulls equipped for combat, recon, and research)
        -   Dermal Plating (+1 armor)
    -   Assassin gain Stealth and two abilities from this list:
        -   Ambush (attacks from concealment to do +3 damage)
        -   Reflexes (you always go first and can react when suddenly surprised)
        -   Cypher (you cannot be identified and leave no trace behind when hacking systems)
        -   Tinker (you can attempt to quickly pick a lock, pick a pocket, or disable a physical security system)
    -   Chiurgeon gain Heal and two abilities from this list:
        -   Mend (you can attempt to neutralize poisons, remove diseases, or heal wounds with a few seconds of time)
        -   Skilled(pick two more skills for free)
        -   Magos Biologis(To you, biology is just another form of Technology)
        -   Hardy (+6 HP)
    -   Warrior gain Athletics and two abilities from this list:
        -   Skirmish (+1 damage and worn armor counts as one type lighter)
        -   Tough (+1 armor)
        -   Slay (+2 melee damage)
        -   Smart-Link (+2 ranged damage)
    -   Seeker gain Deception, Drive, and Info. They also choose one from this list:
        -   Lucky(Once per day, turn a miss into a partial success)
        -   Contacts(you always know a guy who knows a guy and can seek aid from one of your many connections)
        -   Scout(When you scout ahead you always spot the target before it spots you) \#\#\#Custom Character\#\#\#

### Custom Character

Distribute either the General Array or Dangerous array to Tuf, Agi, Str, Int, Fel, and Wil. Then chose two skills and two abilities from *any* class.

Custom World, General Array
:   Distribute +2, +1, +1, +0, +0, -1 to Tuf, Agi, Str, Int, Fel, and Wil

Custom World, Dangerous Array
:   Distribute +3, +0, +0, +0, +0, -1 to Tuf, Agi, Str, Int, Fel, and Wil

Skills
:   Athletics, Awareness, Bureaucracy, Deception, Drive, Heal, Info, Leadership, Stealth, Survival, Psyniscience, Tech-Use


