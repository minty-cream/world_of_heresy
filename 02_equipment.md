Equipment
---------

There is no currency. When you need something, roll 2d6+Rank+Rarity. On a 7-9 theres something wrong with it. On a 10+ its fine. On a 12+ Its got something extra.

You start with as many items, with a Base rarity greater than -1, you can carry.

  ------------------------------------------------------------------------
  Item             Base   Details
                   Rarity 
  ---------------- ------ ------------------------------------------------
  *Weapons*        -      -

  Light Weapon     +0     d6. May be wielded as a secondary weapon,
                          allowing you to re-roll damage once per attack.
                          Includes daggers, short swords, and hand axes.

  Martial Weapon   -1     d6+1. Must be wielded in main hand. Includes
                          long swords, hammers, axes, spears etc.

  Great Weapon     -2     d6+2 damage. Uses two hands. Includes two-handed
                          swords, battle-axes and pole arms.

  Power Field      -3     +1d6. Choose a Light, Martial, or Great weapon,
                          this item has a power field in addition to any
                          other effects.

  Small Gun        +0     d6. Includes light pistols, SMGS, hold-out, and
                          las-pistols

  Gun              -1     d6+1 Includes medium and heavy pistols, SMGs,
                          hunting rifles, and lasgun.

  Heavy Gun        -2     2d6. Lascannon, Autogun

  Bolt Gun         -3     Choose a Small, Heavy, or normal solid
                          projectile Gun. It can now use bolt ammunition.

  Autofire         -2     Makes a gun automatic. Make two attacks.

  Grenades         +0     d6+2 damage to an area. Also mines, and
                          explosives.

  Special Grenade  -2     Includes EMP, Hallucinogen, melta, and Krak

  *Armor*          -      -

  Light Armor      +0     Armor 1

  Full Armor       -1     Armor 2. Always has a helmet. Always makes it
                          very hard to run, move quietly, swim, leap etc

  Portable Void    -6     Armor 2.
  Shield Generator        

  Power Armor      -12    Armor 6. Ignores the damage of any weapon
                          without a power field. Power Weapons do not
                          penetrate its armor.

  *Vehicles*       -      -

  Civilian Vehicle +0     60mph 2d6 Armor 3 Inludes Cars, Jeeps, Buses.

  Combat Vehicle   -6     70kph 6d6 Armor 6 Includes Chimera, Mukaali.

  Rare Combat      -24    25kph 12d6. Armor 12 Includes Baneblade, Titan,
  Vehicle                 Lemun Russ Tank.

  *Other*          -      -

  Simple Gear      +3     20ft rope, lockspicks, grapppling hook,
                          flashlight, backpack, restraints, detox sprays,
                          chemsuit, survival kit, combead, etc.

  Advanced Gear    +0     arc-torch, auto-saw, armored briefcase, wetsuit,
                          gas/airmask, surveillance gear, pictocorder,
                          vox-caster, etc.

  Xenos            -3     Choose any item. Its the Xenos variety. Usually
                          comes with "something extra"
  ------------------------------------------------------------------------


