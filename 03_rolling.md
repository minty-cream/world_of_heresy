Rolling The Dice
================

When you attempt something risky, sum 2d6 and add one of your attribute scores, based on the action you’re taking. (The GM will tell you some of the possible consequences before you roll, so you can decide if it’s worth the risk or if you want to revise your action.) A total of 6 or less is a miss; things don’t go well and the risk turns out badly. A total of 7-9 is a partial success; you do it, but there’s some cost, compromise, retribution, harm, etc. A total of 10 or more is a full success; you do it without complications. And a total of 12 or more is a critical success; you do it perfectly to some extra benefit or advantage. Skills: If you have an applicable skill, you can’t miss. A roll of 6 or less counts as a partial success, but with a bigger compromise or complication than a 7-9 result.

THE DIE OF FATE
---------------

Sometimes the GM will roll the die of fate to see how the situation is established. Low numbers are ill-fortune, high numbers are good fortune (or at least not misery). The die of fate might be rolled to establish the weather, indicate a random NPC’s general attitude, or to determine if a wandering monster appears. The GM may also roll the die of fate if the PCs take some action for which sheer chance is the only factor in the outcome. These rules are yours to bend to your will! You may find it natural to expand, redact, and modify them as you your game goes on. We advise keeping an open mind and lively discussion of possibilities at the table.
