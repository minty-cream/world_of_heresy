XP And Ranks
============

You get 1 XP when you uncover heresy, prevent heresy, destroy heresy, or your superiors say so.

  rank   hit dice   skills   attributes    abilities   damage   xp total
  ------ ---------- -------- ------------- ----------- -------- ----------
  1      1+Tuf      1+1      –             2                    0
  2      +1                                                     1
  3                          +1            +1                   3
  4      +1                  +1 (max +3)                        6
  5                                                    +1d6     10
  6      +1         +1                     +1                   15
  7                          +1(max +3)                         21
  8      +1                                                     28
  9                 +1                     +1                   36
  10     +1                  +1 (max +3)               +1d6     45
