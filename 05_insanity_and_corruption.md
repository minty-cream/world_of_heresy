Insanity
--------

When you fail encountering the insane, gain 1d6 points of insanity. You go absolutely insane at 36 Insanity

Corruption
----------

When you fail a willpower check against the Ruinous powers, gain 1d6 points of corruption. You gain mutations every 8 points. You become corrupted beyond repair at 64 points of corruption.
