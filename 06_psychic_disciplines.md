Psychic Disciplines
===================

You start with a number of psychic disciplines equal to your Willpower.

The psychic disciplines are Force, Biomancy, Divination, Pyromancy, Telekinesis, and Telepathy.

When you use a psychic power roll 2d6+Wil ...
...If you have a discipline that matters, on a 6- roll on the Psychic Phenomena Table
...If you do not have a discipline that matters, on a 7-9 roll on the Psychic Phenomena Table, on a 6- roll on the Perils of the Warp Table
