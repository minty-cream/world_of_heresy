Critical Damage
===============

Note: The critical rules are optional, but highly suggested

When a damage dice rolls a 5 or 6, roll 1d6 for body part (if necessary), and 1d6 for effect on the proper list.

Any effect with a `*` means Instant Death

Body Parts: Head (1), Body(2), Left Arm(3), Right Arm(4), Left Leg(5), Right Leg(6)

Rending
-------

Head
:	(1)Decapitation\*, (2)Eye pops out, (3)brain exposed\*, (4)Face is removed, (5)Helmet or ear is lost forever, (6)eyes or goggles are lost forever

Body
:	(1)Blood Splattering Disembowelment\*, (2)Disembowelment\*, (3)Blood Splatter, (4,5,6 with Armor)Armor loses one point. Armor breaks at 0, (4,5,6 without armor) Extreme skin loss

Arm
:	(1)Trigger-gripping Dismemberment\*,(2)Clean Dismemberment\*,(3)Dangling Broken Arm,(4)Loss of fingers,(5)Arm tendons cut and (6). (6)Drop whatever is held

Leg
:	(1)Blood Splattering Dismemberment\*,(2)Clean Dismemberment\*,(3)Dangling Broken Leg, (4)Loss of Kneecap, (5)Loss of Foot, (6)Leg jerked

Impact
------

Head
:	(1)Head splatters\*. Make a another attack., (2)Head Splatters\*, (3)Brain pulverized\*, (4)Skull fracture, (5) Bloody nose, (6) Tinnitus

Body
:	(1)Massive Organ Damage\*, (2)Minor Organ Damage, (3)Broken Ribcage, (4)Knocked Prone, (5)Breathless, (6)Broken Rib

Arm
:	(1)Shrapnel-like Loss of Arm\*, (2)Gorey Dismemberment\*, (3)Shattered Bone, (4)Crushed Hand, (5)Crushing Disarm, (6)Internal Hemmorage

Leg
:	(1)Gorey Dismemberment\*, (2)Bloody Dismemberment, (3)Crushed Leg, (4)Crushed Foot, (5)Broken Leg, (6)Internal Hemmorage

Explosive
---------

On any Explosive (1) effect, All grenades and ammo explodes.

Head
:	(1)Gibbed\*, (2)Crimson Mist\*, (3) Gibbed\*, (4)Blood Fountain\*, (5)Flayed face and loss of eardrums, (6)Temporarily Blind and Deaf

Body
:	(1)Gibbed\*, (2)Burning Disembowelment\*, (3)Scrambled Nervous System, (4)Loss of Chunks of Flesh, (5)Knocked Prone, (6)Loss of Armor or Skin

Arm
:	(1)Bloody Fountain and Dismembered\*, (2)Arm Disintegration\*, (3)Shattered Bone, (4)Shattered Hand, (5)Finger Missiles, (6)Fractured Limb. Drop whatever is held

Leg
:	(1)Bloody Eruption and Dismembered\*, (2)Gorey Geyser and Dismembered\*, (3)Shattered Bone, (4)Full-body Aerial Spin, (5)Cracked Limb, (6)Fall Prone

Energy
------

Head
:	(1)Superheated Brain Explodes\*, (2)Head catches fire\*, (3)Entire head cooked, (4)Flesh burned away, (5)Hair loss, (6)Ear cooked

Body
:	(1)Blackend Corpse\*, (2)Cooked Organs\*, (3)Catch fire and Knocked Prone, (4)Smoking wound and Knocked Prone, (5)Scorched Body, (6)Breathless

Arm
:	(1)Smoking Stump, (2)Limb catches flame, (3)Boiled marrow and Shattered Limb, (4)Fused fingers, (5)Shock and vomit, (6)Numb arm

Leg
:	(1)Smoking Stump, (2)Loss of Bone, (3)Blackened Limb, (4)Blackened Foot, (5)Electric Shock and Scrambled Nervous System, (6)Painful Fusion of Leg and Clothing

