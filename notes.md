Notes for homebrew
==================

Mechanics of Dark Heresy
------------------------

> Everything in this section is organized With a new game in mind

-   Important new stat concepts - Done, needs its own file
    -   Insanity - When you fail encountering the insane, gain 1d6 points of insanity. You go absolutely insane at 36 Insanity
    -   Corruption - When you fail a willpower check against the Ruinous powers, gain 1d6 points of corruption. You gain mutations every 8 points. You become corrupted beyond repair at 64 points of corruption.
-   Chargen - Done
    -   Homeworld
    -   Background
    -   Role
-   Level Up - Done
-   Other - Done
    -   Influence (as opposed to currency) - Done, there is no currency. Instead focus on the acquisition itself and not the money.
-   Optional rules
    -   Phenomena Tables
    -   Crit tables

Major Fluff of 40k
------------------

-   Grim Dark
-   Things die, a lot
-   Highly lethal
-   Psykers are dangerous
-   Heresy is more dangerous
-   Xenos are almost as dangerous
-   People hate Untouchables

Important Things
----------------

-   Use Cover
-   Use Grenades

Questions and Answers
---------------------

-   How do you gain XP (Dark Heresy)?
    -   Uncovering Heresy, Destroying Heresy, Gaining Influence
-   How do you play Rogue Trader in This?
    -   Give the players a ship and replace Influence with Profit Factor.
-   How do you gain XP (Rogue Trader)?
    -   Acquiring Profit Factor.
-   Profit Factor Equivalents.
    -   One Profit Factor an feed a country. Ten Profit Factor can buy the shittiest planet
